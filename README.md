# ROS-Android indoor location publisher

Use an Android phone to publish indoor/outdoor location updates over ROS.  Indoor geolocation uses WiFi, Cell Tower, and Bluetooth via the Android FusedLocationProvider.  Outdoor adds GPS as a data source.  The Android phone requires Internet access.